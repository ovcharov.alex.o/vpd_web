#!/bin/bash

echo "\033[1;33m[!] DJANGO_DEBUG $DJANGO_DEBUG\033[0m"

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000
