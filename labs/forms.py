from django import forms
from .models import Student, Group

# Do form for team submission

class TeamForm(forms.Form):
    name = forms.CharField(max_length=100)

class SubmissionForm(forms.Form):
    telegram = forms.CharField()
    report = forms.FileField()
    comments = forms.CharField()

class GeeksForm(forms.Form): 
    name = forms.CharField() 
    geeks_field = forms.FileField() 
