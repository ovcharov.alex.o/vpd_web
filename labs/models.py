from django.db import models

class Group(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self) -> str:
        return self.name

class Lab(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=100)
    abstract = models.CharField(max_length=300)

    def __str__(self) -> str:
        return f'Лаб. {self.number} {self.name}'

class Student(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=30)
    middle_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

# class Team(models.Model):
#     name = models.CharField(max_length=30)
#     stundents = models.ManyToManyField(Student, through='Participant')

# class Participant(models.Model):
#     student = models.ForeignKey(Student, on_delete=models.CASCADE)
#     team = models.ForeignKey(Team, on_delete=models.CASCADE)

# class Deadline(models.Model):
#     group = models.ForeignKey(Group, on_delete=models.CASCADE)
#     lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
#     date = models.DateTimeField('Deadline')

class Submission(models.Model):
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    contact = models.CharField(max_length=50)
    report = models.FileField(upload_to='labs')
    verified = models.BooleanField(default=False)
    comments = models.TextField(max_length=3000, default='Комментарии')
    review = models.TextField(max_length=3000, default='Комментарии')
    date = models.DateTimeField('Submission date')

class SubmittedStudent(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)

class Defence(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    date = models.DateTimeField('Defence date')
    score = models.FloatField()
    artifacts = models.FileField(default='artifacts')
    comments = models.TextField(max_length=500, default='Комментарии')
