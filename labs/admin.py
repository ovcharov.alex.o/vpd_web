from django.contrib import admin

from .models import Group, Lab, Student, Submission, Defence, SubmittedStudent

# Register your models here.
class DefenceInline(admin.StackedInline):
    model = Defence
    extra = 0
    autocomplete_fields = ['student']

class StudentInline(admin.StackedInline):
    model = SubmittedStudent
    extra = 0
    autocomplete_fields = ['student']

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['id', 'group', 'first_name', 'last_name']
    search_fields = ['id']

@admin.register(Submission)
class SubmissionAdmin(admin.ModelAdmin):
    list_display = ['lab', 'verified', 'contact', 'report', 'date']
    fields = ['lab', 'report', 'contact', 'date', 'comments', 'review', 'verified']
    inlines = [StudentInline, DefenceInline]


admin.site.register(Group)
admin.site.register(Lab)
admin.site.register(Defence)
