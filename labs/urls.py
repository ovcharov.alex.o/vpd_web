from django.urls import path

from .views import ServicesView, SubmitForm

urlpatterns = [
    path('', ServicesView.as_view(), name='index'),
    path('labs/<int:lab_id>/submit', SubmitForm.as_view(), name="submit")
]
