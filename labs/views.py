from django.http import HttpResponse
from django.views.generic import TemplateView, View, ListView
from django.contrib.sites.shortcuts import get_current_site
from django.core.files import File

from .models import Lab, Student, Submission, SubmittedStudent
from .forms import SubmissionForm

from collections import OrderedDict
from datetime import datetime
from pathlib import Path

def write_roman(num):

    roman = OrderedDict()
    roman[1000] = 'm'
    roman[900] = 'cm'
    roman[500] = 'd'
    roman[400] = 'cd'
    roman[100] = 'c'
    roman[90] = 'xc'
    roman[50] = 'l'
    roman[40] = 'xl'
    roman[10] = 'x'
    roman[9] = 'ix'
    roman[5] = 'v'
    roman[4] = 'iv'
    roman[1] = 'i'

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)
            yield roman[r] * x
            num -= (r * x)
            if num <= 0:
                break

    return ''.join([a for a in roman_num(num)])

def index(request):
    return HttpResponse('Hello, world. You\'re at the polls index.')

class ServicesView(TemplateView):
    template_name = 'ServiceList.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        labs = Lab.objects.all()

        roman_lab_number = []
        for lab in labs:
            roman_lab_number.append(write_roman(lab.number))

        context['data'] = zip(labs, roman_lab_number)
        return context

# get_queryset
class SubmitForm(TemplateView):
    template_name = 'SubmitForm.html'
    def get(self, request, lab_id, *args, **kwargs):
        lab = Lab.objects.get(number = lab_id)
        students = Student.objects.all()
        context = self.get_context_data(**kwargs)
        context['lab'] = lab
        context['students'] = students
        return self.render_to_response(context)

    def post(self, request, lab_id, *args, **kwargs):
        lab = Lab.objects.get(number = lab_id)
        context = self.get_context_data(**kwargs)
        context['lab'] = lab
        print(request.POST)
        print(request.FILES)

        sub = Submission()
        sub.lab = lab
        sub.comments = request.POST['comments']
        sub.date = datetime.now()
        sub.contact = request.POST['telegram']

        filename = f'Lab-{lab.number}-{sub.contact}.pdf'
        # handle_uploaded_file(
        #     request.FILES['form-file'],
        #     Path('/Users/R-1221/Documents/Projects/VPD/test_website/vpd_storage') / filename
        # )
        # sub.report = f'http://{get_current_site(request)}/static/{filename}'
        # sub.report = request.FILES['report']

        report_file = File(request.FILES['report'])
        sub.report = report_file

        data = SubmissionForm(request.POST)
        print(
            data.is_valid(),
            data.cleaned_data
        )

        print('---------')
        print('---------')
        print(type(request.FILES['report']))
        print(type(report_file))


        # Save
        sub.save()

        # Add student
        for i in range(int(request.POST['stud-number'])):
            stud = Student.objects.filter(id = int( request.POST[f'stud-id-{i+1}'] ))

            if len(stud) == 0:
                continue
            stud = stud[0]

            SubmittedStudent(student=stud, submission=sub).save()

        return self.render_to_response(context)
    

def handle_uploaded_file(f, dest):
    with open(dest, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
