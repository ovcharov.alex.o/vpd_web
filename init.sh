#!/bin/bash

echo "\033[1;33m[!] DJANGO_DEBUG $DJANGO_DEBUG\033[0m"

# Start server
echo "Migrate"
python3 manage.py migrate

echo "Make migrations"
python3 manage.py makemigrations labs

echo "Database migrations"
python3 manage.py sqlmigrate labs 0001

echo "Migrate"
python3 manage.py migrate

echo "Load labs"
python3 manage.py shell < scripts/load_labs.py

echo "Load students"
python3 manage.py shell < scripts/load_students.py

echo "Collect static"
python3 manage.py collectstatic
