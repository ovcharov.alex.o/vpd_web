from pathlib import Path
import pandas as pd

from labs.models import Lab

lab1_title = 'Построение математической модели двигателя EV3'
lab1_description = 'В работе проводится исследование характеристики разгона двигателя постоянного тока'


lab2_title = 'Получение конструктивной постоянной двигателя'
lab2_description = 'В данной лабораторной работе будет рассмотрена более полная модель двигателя постоянного тока по сравнению с первой лабораторной работой.'


lab3_title = 'Использование регуляторов для управления двигателем'
lab3_description = 'Рассматриваются способы управления двигателем постоянного тока с использованием регуляторов, работающих на основе обратной связи.'


lab4_title = 'Робот с дифференциальным приводом'
lab4_description = 'Проводится исследование методов локализации мобильного робота с дифференциальным приводом и управления им для движения робота в заданную точку.'


lab5_title = 'Метод функции Ляпунова (Проект)'
lab5_description = 'В данной лабораторной работе будет рассмотрен еще один метод управления мобильным роботом из лабораторной работы No4, а также математически доказана его работоспособность.'


titles = [lab1_title, lab2_title, lab3_title, lab4_title, lab5_title]
descritipons = [lab1_description, lab2_description, lab3_description, lab4_description, lab5_description]


for i in range(len(titles)):

    lab = Lab.objects.filter(number = i + 1)
    if len(lab) > 0:
        lab = lab[0]
    else:
        lab = Lab()

    lab.number = i + 1
    lab.name = titles[i]
    lab.abstract = descritipons[i]
    lab.save()
