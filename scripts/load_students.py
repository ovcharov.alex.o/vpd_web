from pathlib import Path
import pandas as pd


from labs.models import Student, Group

BASE_DIR = Path.cwd()


p = BASE_DIR / 'vpd_students/27_02/csv'
files = list(p.glob('*.csv'))

for file in files:
    print(file)

    df = pd.read_csv(file, delimiter=';', header=None)
    data = df.to_numpy()

    for user_info in data:
        itmo_id = user_info[0]
        group_id = user_info[2]
        fio: str = user_info[1]
        fio_data = fio.split(' ')

        first_name = fio_data[-2]
        middle_name = fio_data[-1]
        last_name = ' '.join(fio_data[0:-2])

        if len(Student.objects.filter(id = itmo_id)) > 0:
            print('WARNING!!!! Student with data: ')
            print(itmo_id, '|', last_name, first_name, middle_name, '|', fio)
            print('WARNING!!!! Already exitsts')
            continue

        print(itmo_id, '|', last_name, first_name, middle_name, group_id, '|', fio)

        # Add group
        if len(Group.objects.filter(name = group_id)) == 0:
            g = Group()
            g.name = group_id
            g.save()

        # Get group
        g = Group.objects.get(name = group_id)
        s = Student()
        s.id            = itmo_id
        s.group         = g
        s.first_name    = first_name
        s.middle_name   = middle_name
        s.last_name     = last_name
        s.save()

